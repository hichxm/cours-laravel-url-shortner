<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Short extends Model
{
    use HasFactory;

    public static function generateRandomCode($length = 6)
    {
        $code = Str::random($length);

        if (self::where('code', $code)->exists()) {
            return self::generateRandomCode($length);
        }

        return $code;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
