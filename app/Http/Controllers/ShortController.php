<?php

namespace App\Http\Controllers;

use App\Models\Short;
use Illuminate\Http\Request;

class ShortController extends Controller
{
    
    public function show(Request $request, Short $short)
    {
        return redirect()->to($short->url);
    }
}
