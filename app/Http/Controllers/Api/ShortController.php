<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Short;
use Illuminate\Http\Request;

class ShortController extends Controller
{
    
    public function store(Request $request)
    {
        $validated = $request->validate([
            'url' => ['required', 'url'],
        ]);

        $short = new Short();
        $short->url = $validated['url'];
        $short->code = Short::generateRandomCode();
        $short->user_id = auth()->id();
        $short->save();

        $data = [
            'code' => $short->code,
            'url' => route('short.show', $short),
        ];

        if ($request->wantsXml()) {
            return response()->xml($data);
        }

        return $data;
    }
}
