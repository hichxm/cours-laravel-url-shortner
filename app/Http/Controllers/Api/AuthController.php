<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    
    public function login(Request $request)
    {
        $validated = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (!auth()->attempt($validated)) {
            throw ValidationException::withMessages([
                'credentials' => 'Bas credentials',
            ]);
        }
            
        return auth()->user()->createToken('api');
    }
}
