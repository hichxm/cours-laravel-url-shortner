@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Url</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($shorts as $short)   
                            <tr>
                                <td>{{ $short->code }}</td>
                                <td>{{ $short->url }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
